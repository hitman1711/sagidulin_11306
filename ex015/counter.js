$(function()
{
    var maxLength = $('#myText').attr('maxlength');
    $('#myText').keyup(function()
    {
        var currentLength = $('#myText').val().length;
        $(this).val($(this).val().substr(0, maxLength));
        var count = maxLength - currentLength;
        if (count < 0){
        	count = 0;
        } 
        $('#counterField').html('осталось ' +count + " символов"); 
    })
})