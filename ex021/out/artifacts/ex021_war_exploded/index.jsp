<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.Connection" %>
<%--
  Created by IntelliJ IDEA.
  User: Arthur
  Date: 26.10.14
  Time: 21:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title></title>
  </head>
  <body>
  <%

      Class.forName("org.postgresql.Driver");
      Connection myConn= DriverManager.getConnection("jdbc:postgresql://localhost:5432/bd1",
              "Arthur", "");

  %>
  <header class="jumbotron subhead" id="overview">
      <div class="container">
          <h1 class="lead">Welcome to Posthuman simulator</h1>
      </div>
  </header>
  <div class="row-fluid">
      <div class="span6">
          <div class="hero-unit">
              <p>
                  Posthuman simulator represent our future. Here you get the opportunity to become a god. God in the post-human sense. I believe that we are all living in someone else's model, someone's virtual machine. From this it follows that God is just an administrator (host) of virtual machine.
              </p>
          </div>
      </div>
      <div class="span3">
          <a class="thumbnail" target="_blank">

              <form class="form-signin" action="<%response.sendRedirect("/loginServ");%>" method="post">
                  <h2 class="form-signin-heading">Please sign in</h2>
                  <input type="text" name="usernameField" class="input-block-level" placeholder="Email address">
                  <input type="password" name="passwordField" class="input-block-level" placeholder="Password">
                  <label class="checkbox">
                      <input type="checkbox" value="remember-me"> Remember me
                  </label>
                  <button class="btn btn-large btn-primary" type="submit">Sign in</button>
              </form>
              <a class="btn btn-large btn-primary" action="registration.jsp">Registration
              </a>
          </a>
      </div>
  </div>

  <script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
  <script src="http://code.jquery.com/jquery.js"></script>
  <script src="bootstrap/js/bootstrap.js"></script>
  <script src="js/bootstrap-386.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script src="js/bootstrap-affix.js"></script>
  <script src="js/holder.js"></script>

  </body>
</html>
