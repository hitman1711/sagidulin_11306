<%--
  Created by IntelliJ IDEA.
  User: Arthur
  Date: 07.11.14
  Time: 21:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h2 align="center">Registration</h2>

<div class="lead">
    <form action="<%response.sendRedirect("/registrationServ");%>" method="get" >
        <p>Firstname:</p>
        <input type="text" name="Firstname"/>

        <p>Secondname:</p>
        <input type="text" name="Secondname">

        <p>Username:</p>
        <input type="text" name="Username">

        <p>Password:</p>
        <input type="password" name="password">

        <p>Email</p>
        <input type="email" name="email">

        <div class="btn-group" data-toggle="buttons-radio">
            <p></p>
            <button type="button" class="btn btn-primary">Male</button>
            <button type="button" class="btn btn-primary">Female</button>
        </div>
        <p>Tel.number</p>
        <input type="tel" name="number">

        <p></p>
        <input name="Registration" type="submit" value="Registration">
    </form>
</div>
<script src="js/jquery.js"></script>
<script src="js/bootstrap-386.js"></script>
<script src="js/bootstrap-transition.js"></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-modal.js"></script>
<script src="js/bootstrap-dropdown.js"></script>
<script src="js/bootstrap-scrollspy.js"></script>
<script src="js/bootstrap-tab.js"></script>
<script src="js/bootstrap-tooltip.js"></script>
<script src="js/bootstrap-popover.js"></script>
<script src="js/bootstrap-button.js"></script>
<script src="js/bootstrap-collapse.js"></script>
<script src="js/bootstrap-carousel.js"></script>
<script src="js/bootstrap-typeahead.js"></script>
<script src="js/bootstrap-affix.js"></script>
<script src="js/holder.js"></script>
</body>
</html>
