import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.swing.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class LoginServlet extends HttpServlet {
    Connection conn = null;
    public void init() throws ServletException {
        super.init();
        try{
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/bd1",
                    "Arthur",
                    ""
            );
        }catch (Exception exc){
            System.out.println(exc);
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Md5 md5 = new Md5();
        //String hashedPass = md5.getHash(password);
        if (!username.isEmpty() && !password.isEmpty()) {
            try {
                PreparedStatement s = conn.prepareStatement("select * from users");
                ResultSet rs = s.executeQuery();
                    if (rs.next()) {
                        if(rs.getString("username").equals(username) && rs.getString("password").equals(password)) {
                            Cookie cookie = new Cookie("user", username);
                            cookie.setMaxAge(3600);
                            cookie.setPath("/");
                            response.addCookie(cookie);
                            HttpSession hs = request.getSession();
                            hs.setAttribute("current_user", username);
                            response.sendRedirect("/");
                        } else {
                            System.out.println("username or password is invalid");
                            response.sendRedirect("/login");
                        }
                    }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie userCookie = Helper.getUserCookie(request);
        if (userCookie != null) {
            HttpSession hs = request.getSession();
            hs.setAttribute("current_user", userCookie.getValue());
        }
        if (Helper.current_user(request) != null) {
            response.sendRedirect("/");
        } else {
            response.setContentType("text/html");
            PrintWriter pw = new PrintWriter(response.getOutputStream());
            pw.println("<!doctype html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "<meta charset=\"UTF-8\">\n" +
                    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                    "<title>Welcome</title>\n" +
                    "<link href=\"bootstrap/css/bootstrap.css\" rel=\"stylesheet\" type=\"text/css\">\n" +
                    "<script type=\"text/javascript\" src=\"js/bootstrap-386.js\"></script>\n" +
                    "</head>\n" +
                    "\n" +
                    "<body data-spy=\"scroll\" data-target=\".bs-docs-sidebar\">\n" +
                    "<header class=\"jumbotron subhead\" id=\"overview\">\n" +
                    "  <div class=\"container\">\n" +
                    "    <h1 class=\"lead\">Welcome to Posthuman simulator</h1>\n" +
                    "  </div>\n" +
                    "</header>\n" +
                    "<div class=\"row-fluid\">\n" +
                    "<div class=\"span6\">\n" +
                    "<div class=\"hero-unit\">\n" +
                    "<p>\n" +
                    "Posthuman simulator represent our future. Here you get the opportunity to become a god. God in the post-human sense. I believe that we are all living in someone else's model, someone's virtual machine. From this it follows that God is just an administrator (host) of virtual machine.\n" +
                    " </p>\n" +
                    " </div>\n" +
                    "</div>\n" +
                    "<div class=\"span3\">\n" +
                    "<a class=\"thumbnail\" target=\"_blank\">\n" +
                    "\n" +
                    "      <form class=\"form-signin\" action='/login' method='POST'>\n" +
                    "        <h2 class=\"form-signin-heading\">Please sign in</h2>\n" +
                    "        <input type=\"text\" class=\"input-block-level\" name='username' placeholder=\"Email address\">\n" +
                    "        <input type=\"password\" class=\"input-block-level\" name='password' placeholder=\"Password\">\n" +
                    "        <label class=\"checkbox\">\n" +
                    "          <input type=\"checkbox\" value=\"remember-me\"> Remember me\n" +
                    "        </label>\n" +
                    "        <button class=\"btn btn-large btn-primary\" type=\"submit\" value='send'>Sign in</button>\n" +
                    "      </form>\n" +
                    "<Form name=frm1 action=\"/registration\" method=post>\n" +
                    "        <input type=\"Submit\" value=\"Registration\" name=<%=Registration%>\n" +
                    "</form>" +
                    "</a>\n" +
                    "</a>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "\n" +
                    " <script type=\"text/javascript\" src=\"//platform.twitter.com/widgets.js\"></script>\n" +
                    "    <script src=\"http://code.jquery.com/jquery.js\"></script>\n" +
                    "     <script src=\"bootstrap/js/bootstrap.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-386.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-transition.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-alert.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-modal.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-dropdown.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-scrollspy.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-tab.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-tooltip.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-popover.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-button.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-collapse.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-carousel.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-typeahead.js\"></script>\n" +
                    "    <script src=\"js/bootstrap-affix.js\"></script>\n" +
                    "     <script src=\"js/holder.js\"></script>\n" +
                    "     \n" +
                    "\n" +
                    "</body>\n" +
                    "</html>");
            pw.close();
        }
    }
}