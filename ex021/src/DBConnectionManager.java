/**
 * @author Sagidulin Arthur
 *         11306
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionManager {

    private Connection connection;

    public DBConnectionManager() throws ClassNotFoundException, SQLException{
        Class.forName("org.postgresql.Driver");
        this.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bd1",
                "Arthur", "");
    }

    public Connection getConnection(){
        return this.connection;
    }
}