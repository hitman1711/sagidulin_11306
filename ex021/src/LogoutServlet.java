import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class LogoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (Helper.current_user(request) != null) {
            HttpSession hs = request.getSession();
            Cookie cookie = Helper.getUserCookie(request);
            cookie.setMaxAge(0);
            response.addCookie(cookie);
            hs.removeAttribute("current_user");
        }
        response.sendRedirect("/login");
    }
}