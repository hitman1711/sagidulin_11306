package com.company;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sagidulin Arthur
 *         11306
 *         005
 */
public class ex005 {
    public static void main(String[] args) {
        Random rand = new Random();
        int n=0; int d=0;
        String evenDigitsString = "[02468]{3,6}";
        //Pattern evenDigits = Pattern.compile("[02468]{3,6}");
        do {
            String randD = String.valueOf(rand.nextInt(Integer.MAX_VALUE));
            d++;
            //Matcher validator = evenDigits.matcher(randD);
            if (randD.matches(evenDigitsString)) {
                System.out.println(randD);
                n++;
            }
        } while (n<10);
        System.out.println("Total digits generated: " + d);
    }
}
