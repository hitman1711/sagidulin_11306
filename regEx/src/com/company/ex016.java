/**
 * @author Sagidulin Arthur
 * 11306
 * 016
 */
package com.company;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
public class ex016 {
    public static void main(String[] args) {
        String page;
        String inputString;
        Scanner input = new Scanner(System.in);
        inputString = input.nextLine();
        final String adminValid = "Admin";
        final String profileValid = "profile\\d+";
        final String entryValid = "entry/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";
        //Pattern adminValid = Pattern.compile("Admin", Pattern.CASE_INSENSITIVE);
        //Pattern profileValid = Pattern.compile("profile\\d+", Pattern.CASE_INSENSITIVE);
        //Pattern entryValid = Pattern.compile("entry/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", Pattern.CASE_INSENSITIVE);
        //Pattern errValid = Pattern.compile("^Ad
        //
        //
        //
        //
        //
        //
        //
        //
        //min|profile\\d+|entry[1994-2014]\\-[01-12]\\-[01-31]");
        //Matcher m = adminValid.matcher(inputString);
        //Matcher pr = profileValid.matcher(inputString);
        //Matcher en = entryValid.matcher(inputString);
        //Matcher er = errValid.matcher(inputString);
        if (inputString.matches(adminValid)){
            try {
                page = "<html>" + "\n" + "<head>" + "\n" +
                        "<meta charset =\"UTF-8\"/>" + "\n" +
                        "<title>Admin</title>" + "\n" + "</head>" + "\n" + "<body>" + "\n" +
                        "<h1>Admin</h1>" + "\n" + "</body>" + "\n" + "<html>";
                Files.write(Paths.get(inputString + ".html"), page.getBytes());
                System.out.println("html created");
            } catch (IOException e) {
                System.out.println("shit");
                e.printStackTrace();
            }
        }
        if (inputString.matches(profileValid)){
            try {
                page = "<html>" + "\n" + "<head>" + "\n" +
                        "<meta charset =\"UTF-8\"/>" + "\n" + "<title>" + inputString +
                        "</title>" + "\n" + "</head>" + "\n" + "<body>" + "\n" +
                        "<h1>" + inputString + "</h1>" + "\n" + "</body>" + "\n" + "<html>";
                Files.write(Paths.get(inputString + ".html"), page.getBytes());
                System.out.println("html created");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (inputString.matches(entryValid)){
            try {
                page = "<html>" + "\n" + "<head>" + "\n" +
                        "<meta charset =\"UTF-8\"/>" + "\n" + "<title>" + inputString +
                        "</title>" + "\n" + "</head>" + "\n" + "<body>" + "\n" +
                        "<h1>" + inputString + "</h1>" + "\n" + "</body>" + "\n" + "<html>";
                Files.write(Paths.get(inputString + ".html"), page.getBytes());
                System.out.println("html created");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                page = "<html>" + "\n" + "<head>" + "\n" +
                        "<meta charset =\"UTF-8\"/>" + "\n" +
                        "<title>404 NOT FOUND</title>" + "\n" + "</head>" + "\n" + "<body>" + "\n" +
                        "<h1>ERROR 404</h1>" + "\n" + "</body>" + "\n" + "<html>";
                Files.write(Paths.get("404.html"), page.getBytes());
                System.out.println("html created");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
