package com.company;

import java.util.Random;

/**
 * @author Sagidulin Arthur
 *         11306
 *         007
 */
public class ex007 {
    public static void main(String[] args){
        Random rand = new Random();
        int n=0; int d=0;
        String evenDigitsString = "\\d*?([02468][02468]){2,}\\d*?";
        do {
            String randD = String.valueOf(rand.nextInt(Integer.MAX_VALUE));
            d++;
            if (randD.matches(evenDigitsString)) {
                System.out.println(randD);
                n++;
            }
        } while (n<10);
        System.out.println("Total digits generated: " + d);
    }
}