package com.company;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sagidulin Arthur
 *         11306
 *         008
 */
public class ex008 {
    public static void main(String[] args){
        Random rand = new Random();
        int n=0; int d=0;
        Pattern groupOfTwo = Pattern.compile("\\d*?([02468][02468]){2,}\\d*?");
        do {
            String randD = String.valueOf(rand.nextInt(Integer.MAX_VALUE));
            Matcher validator = groupOfTwo.matcher(randD);
            d++;
            if (validator.find()) {
                System.out.println("found: " + validator.group());
                n++;
            }
        } while (n<10);
        System.out.println("Total digits generated: " + d);
    }

}
