package com.company;

import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sagidulin Arthur
 *         11306
 *         003
 */
public class ex003 {
    public static void main(String[] args) {
        Random rand = new Random();
        int n=0; int counter = 0;
        Pattern evenDigits = Pattern.compile("^\\d*[02468]$");
        do {
            String randD = String.valueOf(rand.nextInt());
            counter++;
            Matcher validator = evenDigits.matcher(randD);
            if (validator.find()) {
                System.out.println(randD);
                n+=1;
            }
        } while (n<10);
        System.out.println("Total generated " +counter);
    }
}