package com.company;

import java.util.Random;

 /**
 * @author Sagidulin Arthur
 *         11306
 *         006
 */
public class ex006 {
    public static void main(String[] args){
        Random rand = new Random();
        int n=0; int d=0;
        String evenDigitsString = "([02468][13579])+";
        do {
            String randD = String.valueOf(rand.nextInt(Integer.MAX_VALUE));
            d++;
            if (randD.matches(evenDigitsString)) {
                System.out.println(randD);
                n++;
            }
        } while (n<10);
        System.out.println("Total digits generated: " + d);
    }
}
