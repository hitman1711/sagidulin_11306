package com.company;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sagidulin Arthur
 * 11306
 * 016
 */
public class ex001 {
    static Scanner sc = new Scanner(System.in);
    static String input = sc.nextLine();
    public static void main(String[] args) throws Exception {
        Pattern phoneValid = Pattern.compile("8\\(\\d{3}\\)\\d{3}\\-\\d{2}\\-\\d{2}");
        Matcher validator = phoneValid.matcher(input);
        if(validator.find()){
            System.out.println("Correct number"+"\n"+input);
        } else throw new Exception("RegEx Error");
    }
}
