package com.company;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class ex010 {
    public static void main(String[] args) {
        try {
            Scanner file = new Scanner(new FileReader("svpply.html"));
            System.out.println("File found, start parsing...");
            Pattern tegs = Pattern.compile("[a-z]*\\.(png|jpg|gif|bmp)");
            do {
                String temp = file.nextLine();
                Matcher finder = tegs.matcher(temp);
                if (finder.find()) {
                    System.out.println(finder.group());
                }
            } while (file.hasNextLine());
        }
        catch(FileNotFoundException e){
            System.out.println("File not found");
            e.printStackTrace();
        }
    }
}
