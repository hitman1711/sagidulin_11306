import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class LogIn extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (!request.getParameter("name").equals("") && !request.getParameter("password").equals("")){
            String name = request.getParameter("name");
            HttpSession hs = request.getSession();
            hs.setAttribute("current_user", name);
            Cookie cookie = new Cookie("stored_user", name);
            cookie.setMaxAge(3600);
            cookie.setPath("/");
            response.addCookie(cookie);
            response.sendRedirect("/");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie cu = helper.getUserFromCookie(request);
        if (cu != null){
            HttpSession hs = request.getSession();
            hs.setAttribute("current_user", cu.getValue());
        }
        if(helper.getCurrentUser(request) ==null){
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.println("<form action='/login' method='POST'>");
            pw.println("<input type='text' name='username'/>");
            pw.println("<input type='password' name='password' />");
            pw.println("<input type='submit' value='send' />");
            pw.println("</form>");
            pw.close();
        } else {
            response.sendRedirect("/");
        }

    }
}
