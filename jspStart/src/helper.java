import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class helper {
    public static String getCurrentUser(HttpServletRequest r) {
        HttpSession hs = r.getSession();
        return (String) hs.getAttribute("current_user");

    }
    public static Cookie getUserFromCookie(HttpServletRequest r) {
        Cookie[] cookies = r.getCookies();
        for(Cookie cookie : cookies){
            if(cookie.getName().equals("stored_user")){
                return  cookie;
            }
        }
        return null;
    }
}
