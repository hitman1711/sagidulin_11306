import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class LogOut extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (helper.getCurrentUser(request)!=null){
            HttpSession hs = request.getSession();
            hs.removeAttribute("current_user");
            Cookie cookie = helper.getUserFromCookie(request);
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        }
        response.sendRedirect("/login");
    }
}
