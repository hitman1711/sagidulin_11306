import com.sun.tools.corba.se.idl.toJavaPortable.Helper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class Feed extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        String cu= helper.getCurrentUser(request);
        if(cu != null){
            PrintWriter pw = response.getWriter();
            pw.println("<h3>Welcome "+cu+"</h3>");
            pw.println("<h1>Recent updates</h1>");
            pw.println("<a href='/logout'>Exit</a>");
            pw.close();
        } else {
            response.sendRedirect("/login");
        }

    }
}
