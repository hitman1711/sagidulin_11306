﻿insert into innerprops (id,name,effect) values(1,'Virtualize molecular level',10);
insert into innerprops (id,name,effect) values(2,'Virtualize atomic level',20);
insert into innerprops (id,name,effect) values(3,'Virtualize subatomic level',30);
insert into innerprops (id,name,effect) values(4,'Virtualize antiparticles',40);
insert into innerprops (id,name,effect) values(5,'Infinite divisibility',80);
insert into innerprops (id,name,effect) values(6,'Disable confinement',100);

insert into outterprops (id,name,effect) values(1,'Virtualize mesosphere',10);
insert into outterprops (id,name,effect) values(2,'Virtualize thermosphere',20);
insert into outterprops (id,name,effect) values(3,'Virtualize exosphere',30);
insert into outterprops (id,name,effect) values(4,'Interstellar space',40);
insert into outterprops (id,name,effect) values(5,'Intergalactic space',50);
insert into outterprops (id,name,effect) values(6,'Virtualize supercluster',100);

insert into memory (id,name,effect) values(0,'Stock Memory',10);
insert into memory (id,name,effect) values(1, 'Enhanced Memory',50);
insert into memory (id,name,effect) values(2, 'Advanced Memory',200);
insert into memory (id,name,effect) values(3, 'Greater Memory',500);
insert into memory (id,name,effect) values(4, 'Super Memory',2000);
insert into memory (id,name,effect) values(5, 'Mega Memory',10000);

insert into cpu (id,name,effect) values(0, 'Stock CPU',17);
insert into cpu (id,name,effect) values(1, 'Exa CPU',18);
insert into cpu (id,name,effect) values(2, 'Zetta CPU',21);
insert into cpu (id,name,effect) values(3, 'Yotta CPU',24);
insert into cpu (id,name,effect) values(4, 'Nonillion CPU',30);
insert into cpu (id,name,effect) values(5, 'Tredecillion CPU',42);
