﻿create table "users"
	(
		id int not null primary key,
		firstname nvarchar(10) NOT NULL,
		secondname nvarchar(10) NOT NULL,
		username nvarchar(10) NOT NULL,
		password nvarchar(10) NOT NULL, 
		email nvarchar(10) NOT NULL,
		tel nvarchar(12) NOT NULL
	);
create table "innerprops"
	(
		id int not null primary key,
		name nvarchar(50) NOT NULL,
		effect double NOT NULL
	);
create table "outterprops"
	(
		id int not null primary key,
		name nvarchar(50) NOT NULL,
		effect double NOT NULL
	);
create table "memory"
	(
		id int not null primary key,
		name nvarchar(50) NOT NULL,
		effect double NOT NULL
	);
create table "cpu"
	(
		id int not null primary key,
		name nvarchar(50) NOT NULL,
		effect double NOT NULL
	);
create table "vms"
	(
		id int not null primary key,
		name nvarchar(20) NOT NULL,
		mem_id int NOT NULL FOREIGN KEY REFERENCES "memory"("id"),
		cpu_id int NOT NULL FOREIGN KEY REFERENCES "cpu"("id"),
		innerprop_id int NOT NULL FOREIGN KEY REFERENCES "innerprops"("id"),
		outterprop_id int NOT NULL FOREIGN KEY REFERENCES "outterprops"("id")
	);