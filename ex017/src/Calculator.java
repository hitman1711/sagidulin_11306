import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class Calculator extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession hs = request.getSession();
        response.setContentType("text/html");
        PrintWriter pw = new PrintWriter(response.getOutputStream());
        pw.print("<form action='http://localhost:8080/otvet' method='POST'>");
        if(hs.getAttribute("err")!=null){
            pw.print("<p><h2><font color=red>"+hs.getAttribute("err")+"</font></h2></p>");
        }
        pw.print("<p>first number:</p>");
        if(hs.getAttribute("first")!=null){
            pw.print("<input type=\"text\" name=\"fNumber\" value=\""+hs.getAttribute("first").toString()+"\" />");
        } else {
            pw.print("<input type=\"text\" name=\"fNumber\" />");
        }
        pw.print("<p>second number:</p>");
        if(hs.getAttribute("second")!=null){
            pw.print("<input type=\"text\" name=\"sNumber\" value=\""+hs.getAttribute("second").toString()+"\" />");
        } else {
            pw.print("<input type=\"text\" name=\"sNumber\" />");
        }

        pw.print("<p><select name = 'param'>\n"+ "<option value = 'sum'>+</option>\n"+
                "<option value = 'subtr'>-</option>\n"+"<option value = 'mult'>*</option>\n"+
                "<option value = 'div'>/</option>\n"+"<option value = 'sin'>sin</option>\n"+
                "<option value = 'cos'>cos</option>\n"+"<option value = 'ln'>ln</option>\n"+
                "<option value = 'exp'>exp</option>\n");
        pw.print("<input type=\"submit\" name=\"solve\">");
        pw.print("</form>");
        pw.close();
    }
}
