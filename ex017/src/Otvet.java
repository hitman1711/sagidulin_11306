import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class Otvet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession hs = request.getSession();
        String error;
        response.setContentType("text/html");
        PrintWriter pw = new PrintWriter(response.getOutputStream());
        String op = request.getParameter("param");
        Pattern digits = Pattern.compile("[-+]?[0-9]*\\.?[0-9]+");
        Matcher fValidator = digits.matcher(request.getParameter("fNumber"));
        Matcher sValidator = digits.matcher(request.getParameter("sNumber"));
        if(fValidator.matches()){
            if(!op.equals("") && request.getParameter("fNumber")!=null && request.getParameter("sNumber")!=null){
                double firstNum = Double.parseDouble(request.getParameter("fNumber"));
                hs.setAttribute("first", firstNum);

                if(op.equals("sum")){
                    if(request.getParameter("fNumber").length()<1 || request.getParameter("sNumber").length()<1 || !sValidator.matches()){
                        error="Write number!";
                        hs.setAttribute("err",error);
                        response.sendRedirect("/calc");
                    } else {
                        double secondNum = Double.parseDouble(request.getParameter("sNumber"));
                        hs.setAttribute("second", secondNum);
                        double s = firstNum + secondNum;
                        pw.print("<h3>" + s + "</h3>");
                    }
                }
                if(op.equals("subtr")){
                    if(request.getParameter("fNumber").length()<1 || request.getParameter("sNumber").length()<1 || !sValidator.matches()){
                        error="Write second number!";
                        hs.setAttribute("err",error);
                        response.sendRedirect("/calc");
                    } else {
                        double secondNum = Double.parseDouble(request.getParameter("sNumber"));
                        hs.setAttribute("second", secondNum);
                        double s = firstNum - secondNum;
                        pw.print("<h3>" + s + "</h3>");
                    }
                }
                if(op.equals("mult")){
                    if(request.getParameter("fNumber").length()<1 || request.getParameter("sNumber").length()<1 || !sValidator.matches()){
                        error="Write second number!";
                        hs.setAttribute("err",error);
                        response.sendRedirect("/calc");
                    } else {
                        double secondNum = Double.parseDouble(request.getParameter("sNumber"));
                        hs.setAttribute("second", secondNum);
                        double s = firstNum * secondNum;
                        pw.print("<h3>" + s + "</h3>");
                    }
                }
                if(op.equals("div")){
                    double secondNum = Double.parseDouble(request.getParameter("sNumber"));
                    hs.setAttribute("second", secondNum);
                    if(request.getParameter("fNumber").length()<1 || request.getParameter("sNumber").length()<1 || !sValidator.matches()){
                        error="write number!";
                        hs.setAttribute("err",error);
                        response.sendRedirect("/calc");
                    } else {
                        if(secondNum==0) {
                            error="Divide by zero!";
                            hs.setAttribute("err",error);
                            response.sendRedirect("/calc");
                        }
                        double s = firstNum / secondNum;
                        pw.print("<h3>" + s + "</h3>");
                    }
                }
                if(op.equals("sin")){
                    if(request.getParameter("sNumber").length()<1){
                        pw.print("<h3>"+Math.sin(firstNum)+"</h3>");
                    } else {
                        error="Delete second number!";
                        hs.setAttribute("err",error);
                        response.sendRedirect("/calc");
                    }
                }
                if(op.equals("cos")){
                    if(request.getParameter("sNumber").length()<1){
                        pw.print("<h3>"+Math.cos(firstNum)+"</h3>");
                    } else {
                        error="Delete second number!";
                        hs.setAttribute("err",error);
                        response.sendRedirect("/calc");
                    }
                }
                if(op.equals("ln")){
                    if(request.getParameter("sNumber").length()<1){
                        pw.print("<h3>"+Math.log(firstNum)+"</h3>");
                    } else {
                        error="Delete second number!";
                        hs.setAttribute("err",error);
                        response.sendRedirect("/calc");
                    }
                }
                if(op.equals("exp")){
                    if(request.getParameter("sNumber").length()<1){
                        pw.print("<h3>"+Math.exp(firstNum)+"</h3>");
                    } else {
                        error="Delete second number!";
                        hs.setAttribute("err",error);
                        response.sendRedirect("/calc");
                    }
                }
                pw.close();
            } else {
                response.sendRedirect("/calc");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
