import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class Table extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = new PrintWriter(response.getOutputStream());
        Date d = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        pw.print("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <title>About me</title>\n" +
                "    <link href=\"../bootstrap.css\" type='text/css' rel=\"stylesheet\" media=\"screen\"/>\n" +
                "    <link rel='stylesheet' type='text/css' href='../style.css' />" +
                "</head>\n" +
                "<body>");
        pw.print("<div class=\"well well-small\">\n" +
                format1.format(d)+
                "<ul class=\"nav nav-pills\">\n" +
                " <li><a href=\"http://localhost:8080/main\">About me</a></li>\n" +
                " <li><a href=\"http://localhost:8080/calc\">My online calculator</a></li>\n" +
                " <li class=\"active\"><a href=\"http://localhost:8080/tabl\">My multiplication table</a></li>\n" +
                "</ul>\n" +
                "</div>\n");
        pw.print("<form method=\"POST\" action=\"http://localhost:8080/tabRes\">\n" +
                "      <input type=\"number\" name=\"numb\"/>\n" +
                "      <input type=\"submit\" name='go'/>"+
                 "    </form>  ");
        pw.print("</body></html>");
        pw.close();
    }
}
