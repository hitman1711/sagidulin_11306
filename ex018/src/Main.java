import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class Main extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = new PrintWriter(response.getOutputStream());
        Date d = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        pw.print("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <title>About me</title>\n" +
                "    <link href=\"../bootstrap.css\" type='text/css' rel=\"stylesheet\" media=\"screen\"/>\n" +
                "    <link rel='stylesheet' type='text/css' href='../style.css' />" +
                "</head>\n" +
                "<body>");

        pw.print("<div class=\"container-fluid\">\n" +
                    "<div class=\"well well-small\">\n" +
                format1.format(d)+
                        "<ul class=\"nav nav-pills\">\n" +
                            " <li class=\"active\"><a href=\"http://localhost:8080/main\">About me</a></li>\n" +
                            " <li><a href=\"http://localhost:8080/calc\">My online calculator</a></li>\n" +
                            " <li><a href=\"http://localhost:8080/tabl\">My multiplication table</a></li>\n" +
                        "</ul>\n" +
                    "</div>\n" +
                "<table border='1' class=\"table\">\n" +
                "<tr>\n" +
                "   <td>Name</td>\n" +
                "   <td>Arthur</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "   <td>Nickname</td>\n" +
                "   <td>hitman</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "   <td>Email</td>\n" +
                "   <td>iwasraisedinacity@gmail.com</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "   <td rowspan='3'>Interests</td>\n" +
                "       <td><a href='http://yandex.ru/yandsearch?text=coding'>coding</a></td>\n"+
                "</tr>\n" +
                "<tr>\n"+
                "<td><a href ='http://yandex.ru/yandsearch?text=music'>music</a></td>\n"+
                "</tr>\n" +
                "<tr>\n"+
                "<td><a href ='http://yandex.ru/yandsearch?text=money'>money</a></td>\n"+
                "</tr>\n" +
                "</table>\n" +
                "\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>");

        pw.close();
    }
}
