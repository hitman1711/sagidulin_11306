import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Teachers> teacherses = new ArrayList<Teachers>();
        ArrayList<Students> studentses = new ArrayList<Students>();
        ArrayList<Schools> schoolses = new ArrayList<Schools>();
        ArrayList<Classes> classeses = new ArrayList<Classes>();
        ArrayList<Attendance> attendances = new ArrayList<Attendance>();

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Драйвер не найден", JOptionPane.OK_OPTION);
        }
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bd1",
                    "Arthur", "");
            PreparedStatement s = conn.prepareStatement("select * from schools");
            ResultSet rs = s.executeQuery();
            rs = s.executeQuery();
            while (rs.next()) {
                Schools sc = new Schools();
                sc.id = rs.getInt("id");
                sc.name = rs.getString("name");
                schoolses.add(sc);
            }

            s = conn.prepareStatement("select * from teachers");
            rs = s.executeQuery();
            while (rs.next()) {
                Teachers tc = new Teachers();
                tc.id = rs.getInt("id");
                tc.name = rs.getString("name");
                tc.school_id = rs.getInt("school_id");
                for (Schools ss : schoolses) {
                    if (ss.id == tc.school_id)
                        tc.school = ss;
                }
                teacherses.add(tc);
            }
            s = conn.prepareStatement("select * from students");
            rs = s.executeQuery();
            while (rs.next()) {
                Students st = new Students();
                st.id = rs.getInt("id");
                st.name = rs.getString("name");
                studentses.add(st);
            }

            s = conn.prepareStatement("select * from classes");
            rs = s.executeQuery();
            while (rs.next()) {
                Classes cl = new Classes();
                cl.id = rs.getInt("id");
                cl.name = rs.getString("name");
                classeses.add(cl);
            }
            s = conn.prepareStatement("select * from attendance");
            rs = s.executeQuery();
            while (rs.next()) {
                Attendance at = new Attendance();
                at.year = rs.getInt("year");
                int ci = rs.getInt("class_id");
                int ti = rs.getInt("teacher_id");
                int si = rs.getInt("student_id");
                for(Classes cc : classeses){
                    if(cc.id==ci)
                        at.classes=cc;
                }
                for(Teachers tt : teacherses){
                    if(tt.id==ti)
                        at.teachers=tt;
                }
                for(Students ss : studentses){
                    if(ss.id == si)
                        at.students=ss;
                }
                attendances.add(at);
            }

            for (int i = 0; i < attendances.size(); i++) {
                System.out.println(attendances.get(i));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
        } finally {
            try {
                if (conn != null && !conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
            }

        }
    }
}
