/**
 * @author Sagidulin Arthur
 *         11306
 */
public class Attendance {
    int year;
    Teachers teachers;
    Students students;
    Classes classes;

    @Override
    public String toString() {
        return classes.name+" "+teachers.name+" "+year+" "+students.name;
    }
}
