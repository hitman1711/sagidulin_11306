import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.swing.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class LoginServlet extends HttpServlet {
    Connection conn = null;
    public void init() throws ServletException {
        super.init();
        try{
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/bd1",
                    "Arthur",
                    ""
            );
        }catch (Exception exc){
            System.out.println(exc);
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Md5 md5 = new Md5();
        //String hashedPass = md5.getHash(password);
        if (!username.isEmpty() && !password.isEmpty()) {
            try {
                PreparedStatement s = conn.prepareStatement("select * from users");
                ResultSet rs = s.executeQuery();
                if (rs.next()) {
                    if(rs.getString("username").equals(username) && rs.getString("password").equals(password)) {
//                        Cookie cookie = new Cookie("user", username);
//                        cookie.setMaxAge(3600);
//                        cookie.setPath("/");
//                        response.addCookie(cookie);
                        HttpSession hs = request.getSession();
                        hs.setAttribute("current_user", username);
                        response.sendRedirect("/");
                    } else {
                        System.out.println("username or password is invalid");
                        response.sendRedirect("/login");
                    }
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (Helper.current_user(request) != null) {
            response.sendRedirect("/");
        } else {
            response.sendRedirect("/login");
        }
    }
}