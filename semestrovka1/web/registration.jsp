<%--
  Created by IntelliJ IDEA.
  User: Arthur
  Date: 07.11.14
  Time: 21:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <link href="../bootstrap.css" type='text/css' rel="stylesheet" media="screen"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        body {
            background-color: #525252;
        }

        .centered-form {
            margin-top: 60px;
        }

        .centered-form .panel {
            background: rgba(255, 255, 255, 0.8);
            box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
        }
    </style>
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Registration</h3>
                </div>
                <div class="panel-body">

                    <form class="form-signin" action="regProcess.jsp" method="post" role="form">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="Firstname" id="inputFN" class="form-control"
                                           placeholder="Firstname"
                                           required
                                           autofocus>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="Secondname" id="inputSN" class="form-control"
                                           placeholder="Secondname"
                                           required>
                                </div>
                            </div>
                        </div>
                        <label for="inputUN" class="sr-only">Username</label>
                        <input type="text" name="Username" id="inputUN" class="form-control" placeholder="Username"
                               required>

                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password" name="password" id="inputPassword" class="form-control"
                               placeholder="Password"
                               required>

                        <label for="inputEmail" class="sr-only">Email</label>
                        <input type="email" name="email" id="inputEmail" class="form-control"
                               placeholder="Email address" required>
                        <br>
                        <button type="submit" class="btn btn-primary">Register</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>
</body>
</html>
