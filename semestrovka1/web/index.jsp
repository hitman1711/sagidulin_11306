<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.Connection" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Start Page</title>
  <link href="../bootstrap.css" type='text/css' rel="stylesheet" media="screen"/>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">PostSimulator</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <form class="navbar-form navbar-right" role="form" action="login.jsp" method="post">
        <div class="form-group">
          <input type="text" name="usernameField" placeholder="Email" class="form-control">
        </div>
        <div class="form-group">
          <input type="password" name="passwordField" placeholder="Password" class="form-control">
        </div>
        <button type="submit" class="btn btn-success">Log in</button>
      </form>
    </div>
  </div>
</nav>

<div class="jumbotron">
  <div class="container">
    <h1>Welcome to Posthuman simulator</h1>
    <p>Posthuman simulator represent our future. Here you get the opportunity to become a god. God in the post-human sense. I believe that we are all living in someone else's model, someone's virtual machine. From this it follows that God is just an administrator (host) of virtual machine.</p>
    <p><a class="btn btn-primary btn-lg" href="registration.jsp" role="button">Registration &raquo;</a></p>
  </div>
</div>
</body>
</html>
