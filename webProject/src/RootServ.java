import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class RootServ extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cs = request.getCookies();
        response.setContentType("text/html");
        if (cs != null) {
            HttpSession hs = request.getSession();
            for (int i = 0; i < cs.length; i++) {
                if (cs[i].equals(hs.getAttribute("username"))) {
                    PrintWriter pw = new PrintWriter(response.getOutputStream());
                    pw.print("<h1>" + hs.getAttribute("username") + "</h1>");
                    pw.close();
                }
            }
        } else {
            response.sendRedirect("/login");
        }
    }
}
