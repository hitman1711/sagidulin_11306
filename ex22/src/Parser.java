import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class Parser {
    public static void main(String[] args) throws IOException {
        ArrayList links = new ArrayList();
        ArrayList names = new ArrayList();
        Pattern aTag = Pattern.compile("<a\\b[^>]*>(.*?)</a>");
        Pattern hrefTag = Pattern.compile("href=\\\"(.*\\.[a-z0-9]{3})\\\"");
        //Pattern pdfLinks = Pattern.compile("");
        Matcher aMatcher = aTag.matcher(TestStrings.londonPageSegment);
        Matcher hrefMatcher;
        while (aMatcher.find()){
            String str = aMatcher.group();
            hrefMatcher=hrefTag.matcher(str);
            while (hrefMatcher.find()){
                String link = hrefMatcher.group(1);
                links.add(link);
                names.add(link.split("/")[link.split("/").length-1]);
            }
        }
        System.out.println("Parsed link list: ");
        for (int i = 0; i < links.size(); i++) {
            System.out.println(links.get(i));
        }
        System.out.println();
        System.out.println("Initialize networking services...");
        for (int i = 0; i < links.size(); i++) {
            URL url = new URL((String) links.get(i));
            System.out.println("name="+names.get(i)+"   Size="+Networking.getFileSize(url)+" bytes");
            System.out.println("Start downloading...");
            Networking.saveUrl((String)names.get(i),url);
            System.out.println("download succesfully");
            System.out.println();
        }
    }
}
