import java.awt.*;

/**
 * @author Sagidulin Arthur
 *         11306
 */
class TronPlayer {
    Label l1;
    public static int DOWN = 0;
    public static int LEFT = 1;
    public static int UP = 2;
    public static int RIGHT = 3;
    public int dir;
    int headX;
    int headY;
    TronMatrix tm;

    public TronPlayer(int startx, int starty, int startdir, TronMatrix starttm) {
        headX = startx;
        headY = starty;
        dir = startdir;
        tm = starttm;
    }

    public int getDir(){
        return dir;
    }

    public void step(Graphics g) {
        int tx = headX, ty = headY;
        switch (dir) {
            case 0:
                headY++;
                break;
            case 1:
                headX--;
                break;
            case 2:
                headY--;
                break;
            case 3:
                headX++;
                break;
        }

        if ((headX < 0) || (headY < 0) || (headX == tm.fwidth) || (headY == tm.fheight) ||
                (tm.matrix[headY][headX] == 1)) {
            headX = tx;
            headY = ty;
            tm.state = TronMatrix.GAME_OVER;
        } else {
            tm.matrix[headY][headX] = 1;
            tm.drawCell(g, headY, headX);
        }
    }
}