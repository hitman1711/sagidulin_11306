import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class UserServer extends Thread {

    TronPlayer[] tpTemp;

    Socket p;
    InputStream is;
    OutputStream os;


    public UserServer(int port,TronPlayer[] tp){
        tpTemp=tp;

        ServerSocket server = null;

        try {
            server = new ServerSocket(port);
            p = server.accept();

            server.close();
            is = p.getInputStream();
            os = p.getOutputStream();

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.start();
    }

    public UserServer(String ip,int port,TronPlayer[] tp){
        tpTemp=tp;

        try {
            p = new Socket(ip,port);
            is = p.getInputStream();
            os = p.getOutputStream();

        } catch (IOException e) {
            e.printStackTrace();
        }
        this.start();
    }


    public void run() {

        int i = Math.abs(TronMatrix.tronPlayerID - 1);

        while( true ){

            try {
                os.write(tpTemp[TronMatrix.tronPlayerID].getDir());
                tpTemp[i].dir = is.read();
                Thread.sleep(100);

            } catch (Exception e) {
                try {
                    p.close();
                    stop();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }

        }

    }
}
