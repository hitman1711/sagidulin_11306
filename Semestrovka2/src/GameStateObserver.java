import java.util.Observable;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class GameStateObserver extends Observable {
    private int state;
    public void setState(int s) {
        synchronized (this) {
            this.state = s;
        }
        setChanged();
        notifyObservers();
    }
    public synchronized int getState() {
        return state;
    }
}
