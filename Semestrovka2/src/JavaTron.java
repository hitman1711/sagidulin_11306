import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;

/**
 * @author Sagidulin Arthur
 *         11306
 */

class JavaTron extends Frame {

    public static Frame f;
    public static TronMatrix tm;

    JavaTron(String s,int startXX,int startYY,Color color) {
        super(s);
        setSize(640, 480);
        setVisible(true);
        setMenuBar(Menu.mbar);

        add(tm);
        tm.setVisible(true);
        tm.repaint();
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
                setVisible(false);
                dispose();
                f.setVisible(true);

            }
        });

        Timer timer = new Timer();
        timer.schedule(new TronTimer(tm), 200, 200);



    }

    public static void main(String[] args) {


        tm = new TronMatrix(60, 40);

        //new JavaTron("Tron",1,1,Color.BLUE);
        f = new Menu();
    }



}