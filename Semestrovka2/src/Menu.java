import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

/**
 * @author Sagidulin Arthur
 *         11306
 */
public class Menu extends Frame {
    public static Frame f1;
    public static MenuBar mbar;
    public static java.awt.Menu menu;
    public static MenuItem m1, m2;
    Label l, l1, myip;
    TextField nick, ip, port;
    Button mp, set, exit, back, connect, create;

    public Menu() {
        initMenu();
    }


    public void initMenu() {
        removeAll();
        repaint();
        setTitle("TRON");
        setVisible(true);
        setLayout(new GridLayout(1, 3));
        mbar = new MenuBar();
        menu = new java.awt.Menu("Game");
        m1 = new MenuItem("Main menu");
        m2 = new MenuItem("Exit");
        menu.add(m1);

        menu.add(m2);
        mbar.add(menu);
        setMenuBar(mbar);
        setBackground(Color.BLUE);
        mp = new Button("Multiplayer");
        set = new Button("Settings");
        exit = new Button("Exit");
        mp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                mpAction(e);
            }
        });
        set.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                setAction(e);
            }
        });
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                exitAction(e);
            }
        });
        m1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                initMenu();
            }
        });
        m2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                setVisible(false);
                dispose();
                System.exit(0);
            }
        });
        add(mp);
        add(set);
        add(exit);
        pack();

    }

    private void mpAction(MouseEvent e) {

        removeAll();
        setLayout(new GridLayout(3, 3));
        l1 = new Label();
        l1.setText("Your ip:");
        myip = new Label();
        MyIP ip1 = new MyIP();
        try {
            myip.setText(ip1.getIP() + "  ");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        create = new Button("Create");
        l = new Label();
        l.setText("Host ip & port:");
        ip = new TextField();
        ip.setText("127.0.0.1");
        port = new TextField();
        port.setText("1234");
        connect = new Button("Connect");
        back = new Button("Back");
        add(l1);
        add(myip);
        add(create);
        add(l);
        add(ip);
        add(port);
        add(connect);
        add(back);
        pack();
        repaint();

        create.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                TronMatrix.tronPlayerID = 0;

                UserServer serv = new UserServer(Integer.parseInt(port.getText()),TronMatrix.tronPlayers);

                f1 = new JavaTron("TRON",5,5,Color.RED);
                setVisible(false);

            }
        });
        back.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                initMenu();
            }
        });

        connect.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {

                TronMatrix.tronPlayerID = 1;
                UserServer serv = new UserServer(ip.getText(),Integer.parseInt(port.getText()),TronMatrix.tronPlayers);

                Frame f1 = new JavaTron("TRON",55,5,Color.BLUE);
                setVisible(false);
            }
        });
        port.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                textFieldKeyTyped(e, 4, port);
            }
        });
        ip.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                textFieldKeyTyped(e, 15, ip);
            }
        });
    }

    private void setAction(MouseEvent e) {
        removeAll();
        repaint();
        setLayout(new FlowLayout());
        l = new Label();
        l.setText("Write your nickname(max 7): ");
        nick = new TextField();
        nick.setColumns(12);
        nick.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                textFieldKeyTyped(e, 7, nick);
            }
        });
        nick.setText("Player");
        back = new Button("Back");
        back.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                initMenu();
            }
        });

        add(l);
        add(nick);
        add(back);
        pack();

    }

    private void textFieldKeyTyped(KeyEvent e, int border, TextField tf) {
        if (tf.getText().length() >= border) {
            e.consume();
        }
    }

    public void exitAction(MouseEvent e) {
        setVisible(false);
        dispose();
        System.exit(0);
    }

}
