import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import jdk.nashorn.internal.runtime.Property;
import jdk.nashorn.internal.runtime.PropertyListener;
import jdk.nashorn.internal.runtime.ScriptObject;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.util.Observer;
import java.util.Timer;


/**
 * @author Sagidulin Arthur
 *         11306
 */
class TronMatrix extends Canvas {

    public static final int GAME_RUNNING = 0;
    public static final int GAME_OVER = 1;
    public Color color;
    static int tronPlayerID;
    int matrix[][];
    int fwidth;
    int fheight;
    static int troncount;
    static TronPlayer tronPlayers[];
    int state;


    TronMatrix(int nwidth, int nheight) {
        super();

        matrix = new int[nheight][nwidth];

        setBounds(10, 50, nwidth * 10 + 1, nheight * 10 + 1);
        setBackground(Color.black);
        fwidth = nwidth;
        fheight = nheight;
        troncount = 2;
        tronPlayers = new TronPlayer[troncount];

        tronPlayers[0] = new TronPlayer(0, nheight/2, TronPlayer.RIGHT, this);
        tronPlayers[1] = new TronPlayer(nheight, nheight/2, TronPlayer.LEFT, this);




        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (state == TronMatrix.GAME_RUNNING) {
                    switch (e.getKeyCode()) {
                        case KeyEvent.VK_S:
                            tronPlayers[tronPlayerID].dir = TronPlayer.DOWN;
                            break;
                        case KeyEvent.VK_W:
                            tronPlayers[tronPlayerID].dir = TronPlayer.UP;
                            break;
                        case KeyEvent.VK_A:
                            tronPlayers[tronPlayerID].dir = TronPlayer.LEFT;
                            break;
                        case KeyEvent.VK_D:
                            tronPlayers[tronPlayerID].dir = TronPlayer.RIGHT;
                            break;

                    }
                }
            }
        });
        state = GAME_RUNNING;


    }

    public void drawCell(Graphics g, int y, int x) {
        switch (matrix[y][x]) {
            case 0:
                break;
            case 1:
                g.setColor(Color.BLUE);
                break;
        }
        g.fillRect(x * 10, y * 10, 10, 10);

    }

    public void paint(Graphics g) {
        for (int y = 0; y < fheight; y++) {
            for (int x = 0; x < fwidth; x++) {
                drawCell(g, y, x);
            }
        }
    }
}