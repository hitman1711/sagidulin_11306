/**
 * @author Sagidulin Arthur
 *         11306
 */

import java.util.TimerTask;

class TronTimer extends TimerTask {
    TronMatrix tm;

    TronTimer(TronMatrix tm1) {
        tm = tm1;
    }

    public void run() {
        for (int i = 0; i < tm.troncount; i++)
        {
            tm.tronPlayers[i].step(tm.getGraphics());
        }
    }
}
